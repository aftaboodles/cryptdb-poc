const MySQL=require ("./MySQL.js");
const bcrypt = require('bcrypt');
const dbConfig=require("./config.json");

module.exports=class MySQLHelper{

    constructor(){
        let cryptDB=dbConfig['cryptDB'];
        this.mySQL=new MySQL();
        this.mySQL.getMySQLConnection(cryptDB['host'], cryptDB['user'], cryptDB['password'], cryptDB['database'], cryptDB['port']);
    }

    registerUser(req, userCallBack){
        let body=req.body;
        bcrypt.hash(body.userPassword, 10, (err, userPassword)=> {
            if(err){
                userCallBack(err);
            }
            let sqlQuery="INSERT INTO users(user_email, user_password, user_name, user_mobile_no, user_city, user_state, user_country) VALUES('"+body.userEmail+"','"+userPassword+"','"+body.userName+"','"+body.userMobileNo+"','"+body.userCity+"','"+body.userState+"','"+body.userCountry+"');";           
            this.mySQL.executeQuery(sqlQuery, (err, user)=>{
                if(err){
                    userCallBack(err); 
                }
                userCallBack(err, user);
            });
        });
    }

    loginUser(req, userCallBack){
        let body=req.body;
        let sqlQuery="SELECT user_password AS userPassword FROM users WHERE user_email='"+body.userEmail+"'";
        this.mySQL.executeQuery(sqlQuery, (err, userPasswordHash)=>{
            console.log("---err in loginUser---", err, "---userPasswordHash---", userPasswordHash);
            if(err){
                userCallBack(err);
            }
            if(userPasswordHash.length==1){
                Object.keys(userPasswordHash).forEach((key)=> {
                    bcrypt.compare(body.userPassword, userPasswordHash[key]['userPassword'], (err, isLoggedIn)=> {
                        if(err){
                            userCallBack(err);
                        }
                        if(isLoggedIn){
                            let sqlQuery="SELECT user_name AS userName FROM users WHERE user_email='"+body.userEmail+"'";
                            this.mySQL.executeQuery(sqlQuery, (err, user)=>{
                                if(err){
                                    userCallBack(err);
                                }
                                Object.keys(user).forEach((key)=> {
                                    let data={"message":"Successfully logged in", "userName":user[key]['userName'],"status":200}
                                    userCallBack(err, data);
                                });
                            });
                        } else {
                            userCallBack({"message":"Unsuccessful log in", "status":401});
                        }
                    });
                });
            } else {
                userCallBack({"message":"Incorrect email id", "status":401});
            }
        });
    }

    getUsers (req, userCallBack){
        let body=req.body;
        let sqlQuery="SELECT user_id as userId, user_email AS userEmail, user_password AS userPassword, user_name AS userName, user_mobile_no AS userMobileNo, user_city AS userCity, user_state AS userState, user_country AS userCountry FROM users";
        this.mySQL.executeQuery(sqlQuery, (err, users)=>{
            if(err){
                userCallBack(err);
            }
            userCallBack(err, users);
        });
    }

    createPost(req, postCallBack){
        let body=req.body;
        let sqlQuery="INSERT INTO posts(post_title, post_description, post_likes) VALUES('"+body.postTitle+"','"+body.postDescription+"',0)";
        this.mySQL.executeQuery(sqlQuery, (err, post)=>{
            if(err){
                postCallBack(err); 
            }
            postCallBack(err, post);
        });
    }

    updatePost(req, postCallBack){
        let body=req.body;
        let sqlQuery="UPDATE posts SET post_likes=post_likes+1 WHERE post_id="+body.postId;
        this.mySQL.executeQuery(sqlQuery, (err, post)=>{
            if(err){
                postCallBack(err); 
            }
            postCallBack(err, post);
        });
    }

    getPosts(req, postCallBack){
        let body=req.body;
        let sqlQuery="SELECT post_id AS postId, post_title AS postTitle, post_description AS postDescription, post_likes AS postLikes FROM posts";
        this.mySQL.executeQuery(sqlQuery, (err, posts)=>{
            if(err){
                postCallBack(err); 
            }
            postCallBack(err, posts);
        });
    }

    createComment(req, commentCallBack){
        let body=req.body;
        let sqlQuery="INSERT INTO comments(comment, post_id, user_name) VALUES('"+body.comment+"','"+body.postId+"','"+body.userName+"')";
        this.mySQL.executeQuery(sqlQuery, (err, comment)=>{
            if(err){
                commentCallBack(err); 
            }
            commentCallBack(err, comment);
        });
    }

    getComments(req, commentCallBack){
        let query=req.query;
        let sqlQuery="SELECT comment, post_id AS postId, user_name AS userName FROM comments WHERE post_id="+query.postId;
        this.mySQL.executeQuery(sqlQuery, (err, comments)=>{
            if(err){
                commentCallBack(err); 
            }
            commentCallBack(err, comments);
        });
    }
}