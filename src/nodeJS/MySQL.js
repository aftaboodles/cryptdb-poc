var mysql = require('mysql');

module.exports =class MySQL{

    constructor(){
    }

    getMySQLConnection(host, user, password, database, port="3306"){
        let databaseCredebtials={ host: host, user: user, password: password, database:database, port:port };
        this.connection=mysql.createConnection(databaseCredebtials);
        if(this.connection){
            this.connection.connect((err)=> {
                if (err) {
                    throw err;
                }
                console.log("Connected!");
              });
            return true;
        }
        return false;
    }

    executeQuery(sqlQuery, callBack){
        this.connection.query(sqlQuery, (err, resultSet)=>{
            if(err){
                return callBack(err);
            }
            return callBack(err, resultSet);
        });
    }
}