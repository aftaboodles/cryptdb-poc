CREATE DATABASE social_site;
DROP TABLE IF EXISTS users;
CREATE TABLE users(
                    user_id INTEGER PRIMARY KEY AUTO_INCREMENT, 
                    user_email VARCHAR(50),
                    user_password VARCHAR(100),
                    user_name VARCHAR(50),
                    user_mobile_no VARCHAR(10),
                    user_city VARCHAR(50),
                    user_state VARCHAR(50),
                    user_country VARCHAR(50)
                 );
ALTER TABLE user ADD CONSTRAINT unique_email UNIQUE(user_email);

DROP TABLE IF EXISTS posts;
CREATE TABLE posts(
                   post_id INTEGER PRIMARY KEY AUTO_INCREMENT,
                   post_title VARCHAR(100),
                   post_description VARCHAR(500),
                   post_likes INTEGER
);

DROP TABLE IF EXISTS comments;
CREATE TABLE comments(
                        comment_id INTEGER PRIMARY KEY AUTO_INCREMENT,
                        comment VARCHAR(300),
                        user_name VARCHAR(50),
                        post_id INTEGER
);