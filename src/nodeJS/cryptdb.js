const express=require("express");
const cryptDBApp=express();
const bodyParser=require("body-parser");
const bcrypt = require('bcrypt');
const cors=require("cors");
const MySQLHelper=require ("./MySQLHelper.js");

cryptDBApp.use(cors());
cryptDBApp.use(bodyParser.json());

let mySQLHelper=new MySQLHelper();
cryptDBApp.post("/userRegistration", (req, res)=>{
    mySQLHelper.registerUser(req, (err, user)=>{
        if(err){
            res.json(err);
        }
        res.json(user);
    });
});

cryptDBApp.post("/userLogin", (req, res)=>{
    let body=req.body;
    mySQLHelper.loginUser(req, (err, user)=>{
        console.log("---err---", err, "---user---", user);
        if(err){
            console.log("---in error of user login---", err);
            // res.writeHead(401, {'Content-Type': 'application/json'});
            return res.json(err);
        } 
        if(user){
            console.log("---in user of user login---", user);
            res.json(user);
        }
    });
});

cryptDBApp.get("/userListing", (req, res)=>{
    mySQLHelper.getUsers(req, (err, users)=>{
        if(err){
            res.json(err);
        } 
        res.json(users);
    });
});

cryptDBApp.post("/createPost", (req, res)=>{
    mySQLHelper.createPost(req, (err, post)=>{
        if(err){
            res.json(err);
        } 
        res.json(post);
    });
});

cryptDBApp.put("/updatePost", (req, res)=>{
    mySQLHelper.updatePost(req, (err, post)=>{
        if(err){
            res.json(err);
        } 
        res.json(post);
    });
});

cryptDBApp.get("/listPosts", (req, res)=>{
    mySQLHelper.getPosts(req, (err, post)=>{
        if(err){
            res.json(err);
        } 
        res.json(post);
    });
});

cryptDBApp.post("/createComment", (req, res)=>{
    mySQLHelper.createComment(req, (err, comment)=>{
        if(err){
            res.json(err);
        } 
        res.json(comment);
    });
});

cryptDBApp.get("/listComments", (req, res)=>{
    mySQLHelper.getComments(req, (err, comments)=>{
        if(err){
            res.json(err);
        } 
        res.json(comments);
    });
});

cryptDBApp.listen(8085, (err, data)=>{
    console.log("Server is running at http://127.0.0.1:8085");
});

