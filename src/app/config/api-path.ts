export const apiPath = {
    "register":"userRegistration",
    "login":"userLogin",
    "listPosts":"listPosts",
    "createPost":"createPost",
    "updatePost":"updatePost",
    "createComment":"createComment",
    "listComments":"listComments"
  };