import { Injectable } from '@angular/core';
import { environment } from "./../../environments/environment";
import { apiPath } from "./../config/api-path";
import { HttpClient } from '@angular/common/http';
import { RequestHeaderOptionsService } from './request-header-options.service';

@Injectable()
export class AuthorisationService {

  private host:string;
  private httpRequestOptions:any={};

  constructor(private httpClient:HttpClient, private requestHeaderOptionsService:RequestHeaderOptionsService) {
    console.log("--environment--", environment);
    this.host="http://"+environment['host']+":"+environment['port']+environment['routePath'];
    console.log("--host--", this.host);
    this.httpRequestOptions=this.requestHeaderOptionsService.getRequestOptions();
  }

  public getRequest(url:string, httpRequestOptions:any){
    return this.httpClient.get(url, httpRequestOptions);
  }

  public postRequest(url:string, data:object, httpRequestOptions:any){
    return this.httpClient.post(url, data, httpRequestOptions);
  }

  public putRequest(url:string, data:object, httpRequestOptions:any){
    return this.httpClient.put(url, data, httpRequestOptions);
  }

  public getPosts(){
    let url=this.host+apiPath['listPosts'];
    return this.getRequest(url, this.httpRequestOptions);
  }

  public createPost(user:object){
    let url=this.host+apiPath['createPost'];
    return this.postRequest(url, user, this.httpRequestOptions);
  }

  public updatePost(user:object){
    let url=this.host+apiPath['updatePost'];
    return this.putRequest(url, user, this.httpRequestOptions);
  }

  public getComments(post:object){
    let url=this.host+apiPath['listComments']+"?postId="+post['postId'];
    return this.getRequest(url, this.httpRequestOptions);
  }

  public createComment(comment:object){
    let url=this.host+apiPath['createComment'];
    return this.postRequest(url, comment, this.httpRequestOptions);
  }

  public signOut(){

  }

}
