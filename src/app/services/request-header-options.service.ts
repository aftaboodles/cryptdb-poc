import { Injectable } from '@angular/core';
import { RequestOptions, Headers }  from '@angular/http';
import { HttpHeaders } from '@angular/common/http'

@Injectable()
export class RequestHeaderOptionsService {

  constructor() { 
  }

  public getRequestOptions(){
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept':  'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };
    return httpOptions;
  }
}