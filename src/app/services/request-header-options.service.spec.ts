import { TestBed, inject } from '@angular/core/testing';

import { RequestHeaderOptionsService } from './request-header-options.service';

describe('RequestHeaderOptionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RequestHeaderOptionsService]
    });
  });

  it('should be created', inject([RequestHeaderOptionsService], (service: RequestHeaderOptionsService) => {
    expect(service).toBeTruthy();
  }));
});
