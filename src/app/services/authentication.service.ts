import { Injectable } from '@angular/core';
import { environment } from "./../../environments/environment";
import { apiPath } from "./../config/api-path";
import { HttpClient } from '@angular/common/http';
import { RequestHeaderOptionsService } from './request-header-options.service';

@Injectable()
export class AuthenticationService {

  private host:string;
  private httpRequestOptions:any={};

  constructor(private httpClient:HttpClient, private requestHeaderOptionsService:RequestHeaderOptionsService) {
    console.log("--environment--", environment);
    this.host="http://"+environment['host']+":"+environment['port']+environment['routePath'];
    console.log("--host--", this.host);
    this.httpRequestOptions=this.requestHeaderOptionsService.getRequestOptions();
  }

  public postRequest(url:string, data:object, httpRequestOptions:any){
    return this.httpClient.post(url, data, httpRequestOptions);
  }

  public userRegistration(user:object){
    console.log("---user---", user);
    let url=this.host+apiPath['register'];
    return this.postRequest(url, user, this.httpRequestOptions);
  }

  public userLogin(user:object){
    console.log("---user---", user);
    let url=this.host+apiPath['login'];
    return this.postRequest(url, user, this.httpRequestOptions);
  }

}
