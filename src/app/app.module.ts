import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { RegistrationComponent } from './components/authentication/registration/registration.component';
import { LoginComponent } from './components/authentication/login/login.component';
import { FormsModule } from '@angular/forms';
import { AuthenticationService } from './services/authentication.service';
import { RequestHeaderOptionsService } from './services/request-header-options.service';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './components/authorisation/dashboard/dashboard.component';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SignOutComponent } from './components/authorisation/sign-out/sign-out.component';

export const AppRoutes: Routes = [
                                    { path: '',   redirectTo: '/login', pathMatch: 'full' },
                                    { path: 'login', component: LoginComponent },
                                    { path: 'registration', component: RegistrationComponent },
                                    { path: 'sign-out', component: SignOutComponent },
                                    { path: 'dashboard', component: DashboardComponent }
                                  ];

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    LoginComponent,
    DashboardComponent,
    SignOutComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    HttpClientModule,
    ToastModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [AuthenticationService, RequestHeaderOptionsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
