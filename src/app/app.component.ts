import { Component, ViewContainerRef } from '@angular/core';
import { Router, NavigationStart, NavigationError } from '@angular/router';
import { environment } from './../environments/environment'
import { ToastsManager } from 'ng2-toastr/src/toast-manager';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  private states:any=["/registration", "/login"];

  public constructor(private router:Router, private toastsManager:ToastsManager, private viewContainerRef:ViewContainerRef){
    this.toastsManager.setRootViewContainerRef(viewContainerRef);
    localStorage.setItem("states", JSON.stringify(this.states));
    this.router.events.subscribe((e: any) => {
      let url=e.url;
      if(e instanceof NavigationError){
        this.toastsManager.error("Invalid URL "+e.url);        
        this.router.navigate(['/login']);
      } else if(e instanceof NavigationStart){
        let states=JSON.parse(localStorage.getItem("states"));
        console.log("---states----", states);
        let stateExists=states.indexOf(url);
        if(stateExists==-1){
          this.toastsManager.error("Please login");
          this.router.navigate(['/login']);
        }
      }
    });
  }
}
