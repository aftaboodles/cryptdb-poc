import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { AuthorisationService } from "./../../../services/authorisation.service";
import { SignOutComponent } from './../sign-out/sign-out.component'
import { ToastsManager } from 'ng2-toastr/src/toast-manager';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers:[AuthorisationService, SignOutComponent]
})
export class DashboardComponent implements OnInit {

  @ViewChild("postForm") postForm:any;
  @ViewChild("commentForm") commentForm:any;

  private userName:string;

  private isCreatePost:boolean=false;
  private post:any={};
  private posts:any=[];
  private comment:any={};
  private comments:any=[];

  constructor(private authorisationService:AuthorisationService, private signOutComponent:SignOutComponent, private toastsManager:ToastsManager, private router:Router, private viewContainerRef:ViewContainerRef) {
    this.toastsManager.setRootViewContainerRef(viewContainerRef);
    this.userName=localStorage.getItem("userName");
  }

  ngOnInit() {
    this.getPosts();
  }

  public createPost(){
    this.isCreatePost=true;
  }

  public publishPost(){
    this.isCreatePost=false;
    console.log("---post---", this.post);
    this.authorisationService.createPost(this.post).subscribe(
      success=>{
        console.log("===success===", success);
        // this.postForm.reset();
        this.post={};
        this.getPosts();
      },
      error=>{
        console.log("===error===", error);
        // this.postForm.reset();
        this.post={};
      });
  }

  public likePost(post){
    this.authorisationService.updatePost(post).subscribe(
      success=>{
        console.log("===success===", success);
        this.getPosts();
      },
      error=>{
        console.log("===error===", error);
      });
  }

  public getPosts(){
    this.authorisationService.getPosts().subscribe(
      success=>{
        console.log("===success===", success);
        this.posts=success;
        for(let postIndex in this.posts){
          this.getCommentsByPostId(JSON.parse(postIndex), this.posts[postIndex]);
        }
        console.log("===success===", success);
      },
      error=>{
        console.log("===error===", error);
      });
  }

  public getCommentsByPostId(postIndex:number, post:object){
    this.authorisationService.getComments(post).subscribe(
      success=>{
        this.posts[postIndex]['comments']=success;
        console.log("--comments--", this.comments);
      },
      error=>{

      });
  }

  public publishComment(postIndex:number){
    this.comment['userName']=this.userName;
    this.comment['postId']=this.posts[postIndex]['postId'];
    this.comment['comment']=this.posts[postIndex]['comment'];
    this.createComment(postIndex);
  }

  public createComment(postIndex){
    this.authorisationService.createComment(this.comment).subscribe(
      success=>{
        console.log("===success===", success);
        // this.postForm.reset();
        this.comment={};
        this.getCommentsByPostId(postIndex, this.posts[postIndex]);
      },
      error=>{
        console.log("===error===", error);
        this.comment={};
        // this.postForm.reset();
      });
  }

  public signOut(){
    this.toastsManager.success("Successfully logged out");
    let states=["/registration", "/login"];
    localStorage.setItem("states", JSON.stringify(states));
    setTimeout(()=>{
      this.router.navigate(['/login']);          
    }, 500);
  }

}
