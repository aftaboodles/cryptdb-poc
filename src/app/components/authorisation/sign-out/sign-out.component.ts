import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AuthorisationService } from "./../../../services/authorisation.service";
import { ToastsManager } from 'ng2-toastr/src/toast-manager';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-out',
  templateUrl: './sign-out.component.html',
  styleUrls: ['./sign-out.component.css']
})
export class SignOutComponent implements OnInit {

  constructor(private authorisationService:AuthorisationService, private toastsManager:ToastsManager, private viewContainerRef:ViewContainerRef, private router:Router) {
    this.toastsManager.setRootViewContainerRef(viewContainerRef);
  }

  ngOnInit() {
  }

  public signOut(){
    this.toastsManager.success("Successfully logged out");
    let states=["/registration", "/login"];
    localStorage.setItem("states", JSON.stringify(states));
    setTimeout(()=>{
      this.router.navigate(['/login']);          
    }, 500);
  }

}
