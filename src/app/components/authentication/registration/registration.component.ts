import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { AuthenticationService } from './../../../services/authentication.service';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/src/toast-manager';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers:[AuthenticationService]
})
export class RegistrationComponent implements OnInit {

  @ViewChild("registrationForm") registrationForm:any;

  private user={};

  constructor(private authenticationService:AuthenticationService, private router:Router,  private toastsManager:ToastsManager, viewContainerRef:ViewContainerRef) {
    this.toastsManager.setRootViewContainerRef(viewContainerRef);
  }

  ngOnInit() {
  }

  public userRegistration(){
    this.authenticationService.userRegistration(this.user).subscribe(
      success=>{
        console.log("--success--", success);
        this.toastsManager.success("successfully registered");
        this.registrationForm.reset();
        setTimeout(()=>{
          this.router.navigate(['/login']);          
        }, 500);
      },
      error=>{
        console.log("--error--", error);
        this.registrationForm.reset();
        this.toastsManager.error("Not registered");
      });
  }
}
