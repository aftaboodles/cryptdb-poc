import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { AuthenticationService } from './../../../services/authentication.service';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/src/toast-manager';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[AuthenticationService]
})
export class LoginComponent implements OnInit {

  @ViewChild("loginForm") loginForm:any;

  private user={};

  constructor(private router:Router, private authenticationService:AuthenticationService, private toastsManager:ToastsManager, viewContainerRef:ViewContainerRef) {
    this.toastsManager.setRootViewContainerRef(viewContainerRef);
  }

  ngOnInit() {
  }

  public userLogin(){
    this.authenticationService.userLogin(this.user).subscribe(
      success=>{
        console.log("--success--", success);
        localStorage.setItem("userName", success['userName']);
        let states=["/registration", "/login", "/dashboard", "/sign-out"];
        localStorage.setItem("states", JSON.stringify(states));
        this.loginForm.reset();
        this.toastsManager.success(success['message']);
        setTimeout(()=>{
          this.router.navigate(['/dashboard']);          
        }, 500);
      },
      error=>{
        console.log("--error--", error);
        this.loginForm.reset();
        this.toastsManager.error("Please check your credentials");
      }
    );
  }

}
